# dd-plist

## Introduction
> dd-plist is a tool library for parsing and generating property list files.

![Animation](animation_EN.gif)

## How to Install
```shell
ohpm install @ohos/dd-plist
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

1. Import the files required by the property list library to the page.

```
import PropertyListParser from '../plist/PropertyListParser.ets';
```

2. Use the project.

2.1 Define **GlobalContext.ts**.
```
  export class GlobalContext {
      private constructor() {}
      private static instance: GlobalContext;
      private _objects = new Map<string, Object>();
      public static getContext(): GlobalContext {
          if (!GlobalContext.instance) {
            GlobalContext.instance = new GlobalContext();
          }
          return GlobalContext.instance;
      }

      getValue(value: string): Object {
         let result = this._objects.get(value);
         if (!result) {
             throw new Error("this value undefined")
         }
             return result;
         }
      setValue(key: string, objectClass: Object): void {
        this._objects.set(key, objectClass);
      }
}

 2. Initialize GlobalContext in src/main/ets/entryability/EntryAbility.ts.
    if(this.context.filesDir.endsWith('/')){
            GlobalContext.getContext().setValue("path", this.context.filesDir);
        }else{
            GlobalContext.getContext().setValue("path", this.context.filesDir+'/');
    }
```

2.2 Initialize **GlobalContext** in **src/main/ets/entryability/EntryAbility.ts**.
```
    if(this.context.filesDir.endsWith('/')){
            GlobalContext.getContext().setValue("path", this.context.filesDir);
        }else{
            GlobalContext.getContext().setValue("path", this.context.filesDir+'/');
    }
```

2.3 Parse the plist file (property list file).
```
     let filePath = GlobalContext.getContext().getValue("path") + this.xmlArrayFile;
        let buf = new ArrayBuffer(8192);
        // Synchronously read data from the file.
        try {
            stream = fs.createStreamSync(filePath, "r+")
        } catch (err) {
            prompt.showToast({
                message: 'No file found!',
                duration: 1000
            })
        }
        if (!stream) {
            return
        }
        let nread = stream.readSync(buf)
        let temp = new Int8Array(buf);
        buf = temp.subarray(0, nread).buffer;
        let arr: Int8Array = ArrayUtils.uint8Arr2Int8Arr(new Uint8Array(buf))
        PropertyListParser.parseByInt8Array(arr, (obj: NSObject) => {
            if (obj instanceof NSArray) {
                let nsa: NSArray = obj
                this.clear()
                this.parsePlistText = "IntegerValue = " + nsa.getArray().toString()
            }
        })
```

2.4 Convert the message to an XML string.
```
    let root: NSDictionary = new NSDictionary();
    let people: NSArray = new NSArray(2);
    let person1: NSDictionary = new NSDictionary();
    person1.putByObject("Name", "Peter"); //This will become a NSString
    let date = new Date()
    date.setFullYear(2011, 1, 13)
    date.setHours(9, 28)
    person1.putByObject("RegistrationDate", new NSDate(null, null, null, null, date)); //This will become a NSDate
    person1.putByObject("Age", 23); //This will become a NSNumber

    let person2: NSDictionary = new NSDictionary();
    person2.putByObject("Name", "Lisa");
    person2.putByObject("Age", 42);
    person2.putByObject("RegistrationDate", new NSDate(null, null, null, "2010-09-23T12:32:42Z"));

    people.setValue(0, person1);
    people.setValue(1, person2);

    root.put("People", people);

    this.parsePlistText = root.toXMLPropertyList()


```

2.5 Generate an XML file.
```
    let path = GlobalContext.getContext().getValue("path")+'People.plist'
    fs.createStream(path, "w+",function(err, stream){
      if (err != null) {
        console.log("e:" + err)
      } else {
        PropertyListParser.saveAsXMLByStream(root, stream)
        stream.closeSync();
      }
    });
```

## Available APIs
1. Parses the plist file through a file object.
   `PropertyListParser.parseByFile(file: fs.File, func: Function)`
2. Parses the plist file through a file path.
   `PropertyListParser.parse(filePath: string, func?: Function): NSObject`
3. Parses the plist file through a byte array.
   `PropertyListParser.parseByBytes(bytes, func: Function)`
4. Parses the plist file through a file stream.
   `PropertyListParser.parseByStream(input: any, func?: Function): NSObject`
5. Generates a plist file in XML format through NSObject.
   `PropertyListParser.saveAsXML(root: NSObject, file: fs.File)`
6. Generates a plist file stream in XML format through NSObject.
   `PropertyListParser.saveAsXMLToStream(root: NSObject, stream: fs.Stream)`
7. Converts a plist file in another format to a plist file in XML format.
   `PropertyListParser.convertToXml(streamIn :fs.Stream, streamOut :fs.Stream)`
8. Saves an XML file through a file.
   `saveAsXMLByFile(root: NSObject, file: fs.File)`
9. Parses the plist file through a byte array.
   `parseByInt8Array(bytes: Int8Array, func: Function)`

## Constraints

This project has been verified in the following version:

- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

## Directory Structure
````
/dd-plist  
├── entry     # Sample code
├── library  # Library
│   └── src/main/ets/components
│       └── plist  # Service logic code for storing the parsing library
│       └── utils  # Tools used by the parsing library
│   └── index.ets  # External APIs
├── README.md  # Readme                   
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/dd-plist/issues) or a [PR](https://gitee.com/openharmony-sig/dd-plist/pulls).

## License
This project is licensed under [MIT License](https://gitee.com/openharmony-sig/dd-plist/blob/master/LICENSE).
